package cn.gson.vboot.model.pojo;

import lombok.Data;

import java.util.Date;

/**
 * <p>****************************************************************************</p>
 * <p><b>Copyright © 2010-2018 soho team All Rights Reserved<b></p>
 * <ul style="margin:15px;">
 * <li>Description : cn.gson.vboot.model.pojo</li>
 * <li>Version     : 1.0</li>
 * <li>Creation    : 2018年09月08日</li>
 * <li>@author     : ____′↘夏悸</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@Data
public class AcLog {

    private Long id;

    private String module;

    private String action;

    private String exception;

    private Date createDate;

    private Long uid;

    private String ip;

    private User user;
}
