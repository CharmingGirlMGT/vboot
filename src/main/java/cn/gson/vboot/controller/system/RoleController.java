package cn.gson.vboot.controller.system;

import cn.gson.vboot.common.AcLog;
import cn.gson.vboot.common.JsonResult;
import cn.gson.vboot.model.pojo.Role;
import cn.gson.vboot.service.PermissionService;
import cn.gson.vboot.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>****************************************************************************</p>
 * <p><b>Copyright © 2010-2018 soho team All Rights Reserved<b></p>
 * <ul style="margin:15px;">
 * <li>Description : 角色管理</li>
 * <li>Version     : 1.0</li>
 * <li>Creation    : 2018年08月08日</li>
 * <li>@author     : ____′↘夏悸</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@RestController
@RequestMapping("/system/role")
public class RoleController {

    @Autowired
    RoleService roleService;

    @Autowired
    PermissionService permissionService;

    @GetMapping("/list")
    @AcLog(module = "角色", action = "列表")
    public JsonResult list() {
        return JsonResult.success(roleService.list());
    }

    @GetMapping("/get")
    @AcLog(module = "角色", action = "获取", params = "id")
    public JsonResult get(Long id) {
        return JsonResult.success(roleService.getById(id));
    }

    @PostMapping("/updateEnable")
    @AcLog(module = "角色", action = "更新状态", params = {"id", "enable"})
    public JsonResult updateEnable(Long id, Boolean enable) {
        roleService.updateEnable(id, enable);
        return JsonResult.success();
    }

    @PostMapping("/save")
    @AcLog(module = "角色", action = "创建")
    public JsonResult save(@Valid Role role) {
        role.setId(null);
        roleService.saveOrUpdate(role);
        return JsonResult.success();
    }

    /**
     * 更新角色信息
     *
     * @param id   定义出来，表示 更新是 id 一定需要给定值
     * @param role
     */
    @PostMapping("/update")
    @AcLog(module = "角色", action = "更新", params = "id")
    public JsonResult update(@RequestParam long id, @Valid Role role) {
        roleService.saveOrUpdate(role);
        return JsonResult.success();
    }

    @PostMapping("/grant")
    @AcLog(module = "角色", action = "授权", params = "id")
    public JsonResult grant(Long id, Long[] pids) {
        roleService.grant(id, pids);
        return JsonResult.success();
    }

    @GetMapping("/delete")
    @AcLog(module = "角色", action = "删除", params = "id")
    public JsonResult delete(Long id) {
        roleService.deleteById(id);
        return JsonResult.success();
    }

    @GetMapping("/permissionList")
    public JsonResult permissionList(Long id) {
        return JsonResult.success(permissionService.getRolePermissionList(id));
    }
}
