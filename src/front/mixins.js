/**
 *<p>****************************************************************************</p>
 * <p><b>Copyright © 2010-2018 soho team All Rights Reserved<b></p>
 * <ul style="margin:15px;">
 * <li>Description : </li>
 * <li>Version     : 1.0</li>
 * <li>Creation    : 2018年08月02日</li>
 * <li>@author     : ____′↘夏悸</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
import _ from 'lodash';

export default {
  mounted() {
    $('.box', this.$el).boxWidget({});
    $('[data-toggle="tooltip"]', this.$el).tooltip();
    $('[data-widget="tree"]', this.$el).tree();

    if (!_.isEmpty(this.$data)) {
      try {
        this._original_ = _.cloneDeep(this.$data);
      } catch (e) {

      }
    }

    $(document).resize();
  }
};
